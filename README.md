# Econometrics Research - Coivd 19 and Economic Development [Working Project]

Research Supervisor: Dr. Maria Perez-Urdiales

This is an on-going independent research project I am undertaking at Stony Brook University as part of two courses -- ECO 487: Independent Research in Economics and ECO 459: Write Effectively in Economics.
Since this is an on-going project, the code here is incomplete, and the final report/thesis is not there.
However, you can find the R code and the dataset (data.csv) here.

The dataset contains nine variables with 70 observations (country wise). The variables are:
CASES:		Average number of new coronavirus positive cases for a country, on a seven-day rolling basis as of March 1, 2021
HCI:		Human Capital Index: Average levels of education and health of an individual in a country
MEAT:		Meat consumption per capita of a country
PD:		Population Density: Number of people per square kilometer
HYGIENE:	Percentage of the population who has access to basic hygiene services
SANITATION:	Percentage of the population who has access to at least basic sanitation services
WATER:		Percentage of the population who has access to safe water for drinking
SL:		Standard of Living: GDP per capita, PPP adjusted, for current U.S. Dollars
HAQI:		Health Access and Quality Index: It measures the quality and accessibility of the healthcare institutions

Here, I have two research questions:
(1) To find the effect of the hygiene service industry on the spread of the coronavirus.
(2) To find the direct dependence of the hygiene service industry on the standard of living of a country.

